import json
import urllib.request
import csv

# Get data from the url
url = 'https://itunes.apple.com/us/rss/topalbums/limit=100/json'

# Write the headers here since I couldn't figure out how to do it in the loop...
headers = ['itunesID', 'Category', 'Name', 'Artist', 'Link', 'Price', 'Release-Date']
with open('/tmp/jsonTest.csv', 'w') as headersFirst:
    headerWrite = csv.DictWriter(headersFirst, headers)
    headerWrite.writeheader()

with urllib.request.urlopen(url) as readData:
    # Load the json data
    output = json.loads(readData.read())
    # Load the entry List
    entry = output['feed']['entry']

    for record in entry:
        itunesId = (record['id']['attributes']['im:id'])
        category = (record['category']['attributes']['label'])
        name = (record['title']).get('label')
        artist = (record['im:artist']['label'])
        link = (record['link']['attributes']['href'])
        price = (record['im:price']['label'])
        releaseDate = (record['im:releaseDate']['attributes']['label'])

        # Build out the new record
        newRecord = {
            'itunesId': itunesId,
            'category': category,
            'name': name,
            'artist': artist,
            'link': link,
            'price': price,
            'releaseDate': releaseDate

        }
        # Open the file for writing
        with open('/tmp/top100.csv', 'a') as top100File:
            writer = csv.DictWriter(top100File, newRecord.keys())
            writer.writerow(newRecord)
            top100File.close()
