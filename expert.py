import ftplib
import json
import urllib.request
import csv
import os

# Get data from the url
url = 'https://itunes.apple.com/us/rss/topalbums/limit=100/json'

#### Headers must in the order you want for the new csv file until I figure out how to do it in the loop
# Write the headers here since I couldn't figure out how to do it in the loop...
headers = ['itunesID', 'Category', 'Name', 'Artist', 'Link', 'Price', 'Release-Date']
with open('/tmp/top100File.csv', 'w') as headersFirst:
    headerWrite = csv.DictWriter(headersFirst, headers)
    headerWrite.writeheader()

# Headers for the album cover images csv
imageHeaders = ['itunesID', 'smallImageLocation', 'mediumImageLocation', 'largeImageLocation']
with open('/tmp/top100ImageFile.csv', 'w') as imageHeadersFirst:
    imageHeaderWrite = csv.DictWriter(imageHeadersFirst, imageHeaders)
    imageHeaderWrite.writeheader()

with urllib.request.urlopen(url) as readData:
    # Load the json data
    output = json.loads(readData.read())
    # Load the entry List
    entry = output['feed']['entry']

    for record in entry:
        itunesId = (record['id']['attributes']['im:id'])
        category = (record['category']['attributes']['label'])
        name = (record['title']).get('label')
        artist = (record['im:artist']['label'])
        link = (record['link']['attributes']['href'])
        price = (record['im:price']['label'])
        releaseDate = (record['im:releaseDate']['attributes']['label'])

        # Build out the new record
        newRecord = {
            'itunesId': itunesId,
            'category': category,
            'name': name,
            'artist': artist,
            'link': link,
            'price': price,
            'releaseDate': releaseDate

        }
        # Open the file for writing
        with open('/tmp/top100File.csv', 'a') as top100File:
            writer = csv.DictWriter(top100File, newRecord.keys())
            writer.writerow(newRecord)
            top100File.close()

        itunesId = (record['id']['attributes']['im:id'])
        albumdir = ('/tmp/top100AlbumsCovers/' + itunesId + '/')
        pngName = (albumdir + itunesId)
        os.makedirs(albumdir)

        for rec in record['im:image']:
            height = rec["attributes"]['height']
            imageUrl = rec['label']

            if height == '55':
                smallImageLocation = rec['label']
                urllib.request.urlretrieve(smallImageLocation, pngName + '-small.png')
                continue
            elif height == '60':
                mediumImageLocation = rec['label']
                urllib.request.urlretrieve(mediumImageLocation, pngName + '-medium.png')
                continue
            elif height == '170':
                largeImageLocation = rec['label']
                urllib.request.urlretrieve(largeImageLocation, pngName + '-large.png')
                continue
            # Build out the new record
        newImageRecord = {
            'itunesId': itunesId,
            'smallImageLocation': smallImageLocation,
            'mediumImageLocation': mediumImageLocation,
            'largeImageLocation': largeImageLocation
        }

        # Open the file for writing
        with open('/tmp/top100ImageFile.csv', 'a') as top100ImageFile:
            writer = csv.DictWriter(top100ImageFile, newImageRecord.keys())
            writer.writerow(newImageRecord)
            top100ImageFile.close()


# # This should transfer the file to the ftp server, but the readme in the root dir \
# # of the ftp server tells me there is only read access
# session = ftplib.FTP('test.rebex.net', 'demo', 'password')
# fileToSend = open('top100File.csv', 'rb')
# session.cwd('/pub')
# print(session.pwd())
# session.storbinary('STOR /pub/example/top100File.csv', fileToSend)
# session.quit()
# fileToSend.close()
